# 🔖 The Best of "TIL" 🎒

In this project, you will find a curated list of TIL ("Today I learned") repositories, tools, articles and sites. A great many of these were inspired (directly or indirectly) by Josh Branchaud. His work is [well-received on HN](https://news.ycombinator.com/from?site=github.com/jbranchaud) and many people attribute him (or [Simon](https://simonwillison.net/)) in their work.  
 