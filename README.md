# 🔖 The Best of "TIL" 🎒

In this project, you will find a curated list of TIL ("Today I learned") repositories, tools, articles and sites. A great many of these were inspired (directly or indirectly) by Josh Branchaud. His work is [well-received on HN](https://news.ycombinator.com/from?site=github.com/jbranchaud) and many people attribute him (or [Simon](https://simonwillison.net/)) in their work.  
 
## Contents

- [🔖 TIL repos and sites](#-til-repos-and-sites) _27 projects_
- [ℹ️ HOWTO TIL](#-howto-til) _6 projects_

## Explanation
- 🥇🥈🥉&nbsp; Combined project-quality score
- ⭐️&nbsp; Star count from GitHub
- 🐣&nbsp; New project _(less than 6 months old)_
- 💤&nbsp; Inactive project _(6 months no activity)_
- 💀&nbsp; Dead project _(12 months no activity)_
- 📈📉&nbsp; Project is trending up or down
- ➕&nbsp; Project was recently added
- ❗️&nbsp; Warning _(e.g. missing/risky license)_
- 👨‍💻&nbsp; Contributors count from GitHub
- 🔀&nbsp; Fork count from GitHub
- 📋&nbsp; Issue count from GitHub
- ⏱️&nbsp; Last update timestamp on package manager
- 📥&nbsp; Download count from package manager
- 📦&nbsp; Number of dependent projects

<br>

## 🔖 TIL repos and sites

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_This section contains known TIL repositories and sites. Typically, a TIL site will have both a Git repository and the Website that is generated based on the contents of that `git` repo._

<details><summary><b><a href="https://github.com/jbranchaud/til">Josh Branchaud</a></b> (🥇21 ·  ⭐ 13K) - Today I Learned. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/jbranchaud/til) (👨‍💻 17 · 🔀 710 · 📦 21 · 📋 20 - 35% open · ⏱️ 27.01.2024):

	```
	git clone https://github.com/jbranchaud/til
	```
</details>
<details><summary><b><a href="https://til.simonwillison.net/">Simon Willison</a></b> (🥇17 ·  ⭐ 880) - Today I Learned. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/simonw/til) (👨‍💻 16 · 🔀 79 · 📋 67 - 22% open · ⏱️ 28.01.2024):

	```
	git clone https://github.com/simonw/til
	```
</details>
<details><summary><b><a href="https://github.com/hashrocket/tilex">hashrocket/tilex</a></b> (🥇14 ·  ⭐ 490) - Today I Learned. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/hashrocket/tilex) (👨‍💻 46 · 🔀 110 · ⏱️ 06.07.2023):

	```
	git clone https://github.com/hashrocket/tilex
	```
</details>
<details><summary><b><a href="https://github.com/jessesquires/TIL">Jesse Squires</a></b> (🥈11 ·  ⭐ 280) - Things Ive learned and/or things I want to remember. Notes, links,.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/jessesquires/TIL) (👨‍💻 5 · 🔀 8 · ⏱️ 29.12.2023):

	```
	git clone https://github.com/jessesquires/TIL
	```
</details>
<details><summary>Show 23 hidden projects...</summary>

- <b><a href="https://github.com/thoughtbot/til">thoughtbot</a></b> (🥈13 ·  ⭐ 3.9K · 💀) - Today I Learned. <code>❗Unlicensed</code>
- <b><a href="https://github.com/cheese10yun/TIL">cheese10yun/TIL</a></b> (🥈13 ·  ⭐ 860) - Today I Learned. <code>❗Unlicensed</code>
- <b><a href="https://github.com/wajahatkarim3/Today-I-Learned">Wajahat Karim</a></b> (🥈11 ·  ⭐ 330 · 💀) - Today I Learned - A list of all things I learn on daily.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code>
- <b><a href="https://github.com/milooy/TIL">milooy/TIL</a></b> (🥈10 ·  ⭐ 600 · 💀) - Today I Learned. <code>❗Unlicensed</code>
- <b><a href="https://leebyron.com/til/">Lee Byron</a></b> (🥈10 ·  ⭐ 56) - Today I Learned. <code><a href="http://bit.ly/34MBwT8">MIT</a></code>
- <b><a href="https://til.codeinthehole.com/">David Winterbottom</a></b> (🥈6 ·  ⭐ 12) - Today I Learnt ... <code>❗Unlicensed</code>
- <b><a href="https://github.com/secretGeek/today-i-learned-staging">secretGeek/today-i-learned-staging</a></b> (🥈6 ·  ⭐ 6) - the markdown source of today-i-learned. <code>❗Unlicensed</code>
- <b><a href="https://til.devjugal.com/">Jugal Kishore</a></b> (🥈6 ·  ⭐ 4) - Whatever I learned Today will go here. A nice way of.. <code>❗Unlicensed</code>
- <b><a href="https://til.brie.dev">Brie Carranza</a></b> (🥈6 ·  ⭐ 1) - Today I Learned. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code>
- <b><a href="https://til.younho9.dev/">Younho Choo</a></b> (🥉5 ·  ⭐ 10 · 💀) - TIL(Today I Learned). <code><a href="http://bit.ly/34MBwT8">MIT</a></code>
- <b><a href="https://benthecoder.github.io/til/#/">Ben the Coder</a></b> (🥉5 ·  ⭐ 4 · 💤) - Today I Learned. <code>❗Unlicensed</code>
- <b><a href="https://til-engineering.nulogy.com/">Nulogy Engineering TIL</a></b> (🥉5 · 💀) - A microblog for web development. <code><a href="http://bit.ly/34MBwT8">MIT</a></code>
- <b><a href="https://til.daryll.codes/">Daryll Santos</a></b> (🥉4 ·  ⭐ 2) - Today I Learned: To get better, I write down bugfixes,.. <code>❗Unlicensed</code>
- <b><a href="https://betatim.github.io/">Tim Head</a></b> (🥉3 ·  ⭐ 2) - Source of my home on the web: http://betatim.github.io. <code>❗Unlicensed</code>
- <b><a href="https://til.musicscience37.com/">Kenta Kabashima</a></b> (🥉2 · 💀) -  <code>❗Unlicensed</code>
- <b><a href="https://til.kracekumar.com/">Kracekumar R</a></b> (🥉2 · 💀) - TIL - Today I learnt. <code>❗Unlicensed</code>
- <b><a href="https://til.duyet.net/">Duyet Le</a></b> (🥉1 ·  ⭐ 2 · 💀) - Today I learned!. <code>❗Unlicensed</code>
- <b><a href="https://cflynn.us/">Casey Flynn</a></b> (🥉1 ·  ⭐ 2 · 💀) - Organized list of little things I learn. <code>❗Unlicensed</code>
- <b><a href="https://github.com/danielecook/TIL">Daniel E Cook</a></b> (🥉1 · 💀) -  <code>❗Unlicensed</code>
- <b><a href="https://koaning.io/til/">vincent d warmerdam</a></b> (🥉1) - vincent d warmerdam. <code>❗Unlicensed</code>
- <b><a href="https://til.unessa.net/">Ville Säävuori aka Uninen</a></b> -  <code>❗Unlicensed</code>
- <b><a href="https://til.acm.illinois.edu/">ACM@UIUC TIL</a></b> -  <code>❗Unlicensed</code>
- <b><a href="https://graffino.com/til">Graffino</a></b> -  <code>❗Unlicensed</code>
</details>
<br>

## ℹ️ HOWTO TIL

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_This section has suggested tools and approaches for building a TIL site as well as articles, blog posts and other write-ups about TIL sites._

🔗&nbsp;<b><a href="https://hackernoon.com/today-i-learned-ok-technically-yesterday-65c64e1dcb6">Today I Learned — (OK, technically yesterday)</a></b>  

🔗&nbsp;<b><a href="https://betatim.github.io/posts/til-explained/">TIL? Today I learnt! - the explainer post</a></b>  

🔗&nbsp;<b><a href="https://simonwillison.net/2020/Apr/20/self-rewriting-readme/">Using a self-rewriting README powered by GitHub Actions to track TILs</a></b>  

🔗&nbsp;<b><a href="https://cflynn.us/posts/2020-04-26-github-action-til-autoformat-readme">Creating a Reusable GitHub Action to Automatically Format a README for a TIL Repository</a></b>  

<details><summary>Show 2 hidden projects...</summary>

- <b><a href="https://github.com/seokju-na/geeks-diary">Geek's Diary</a></b> (🥇15 ·  ⭐ 700 · 💀) - TIL writing tool for programmer. <code><a href="http://bit.ly/34MBwT8">MIT</a></code>
- <b><a href="https://github.com/danielecook/TIL-Tool">TIL Tool</a></b> (🥉6 ·  ⭐ 5 · 💀) - A tool for creating and indexing Today I Learned (TILs). <code><a href="http://bit.ly/34MBwT8">MIT</a></code>
</details>
---

Maintained with the [best-of generator](https://github.com/best-of-lists/best-of-generator), ✨ and 😻 by [🌈 Brie Carranza 🦄](https://brie.dev).